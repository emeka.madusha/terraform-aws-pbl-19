region = "us-east-1"

vpc_cidr = "10.0.0.0/16"

enable_dns_support = "true"

enable_dns_hostnames = "true"

enable_classiclink = "false"

enable_classiclink_dns_support = "false"

preferred_number_of_public_subnets = 2

preferred_number_of_private_subnets = 4

environment = "dev"

ami-web = "ami-0335b3eed95693e75"

ami-bastion = "ami-023aaf1043ca49dff"

ami-nginx = "ami-01b93a14d729a72b3"

ami-sonar = "ami-0fb80384abeda4d2b"

keypair = "devops-aks-root"

master-password = "devopspblproject"

master-username = "david"

account_no = "216972432112"

tags = {
  Owner-Email     = "emeka.madusha@gmail.com"
  Managed-By      = "Terraform"
  Billing-Account = "216972432112"
}